package models

import (
	"time"
)

type TodoEdit struct {
	ID int
	Title string
	IsDone bool
	Edit bool
}

type TodoAdd struct {
	Title string
}

type Todo struct {
	ID      int
	Title   string
	IsDone  bool
	Created time.Time
}

type TodoPageData struct {
	PageTitle string
	Todos     []Todo
	Edit bool
}