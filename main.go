package main

import (
	"./routes"
	"log"
)

func main() {
	log.Println("listening on http://localhost:8000")
	routes.SetupRoutes()
}

