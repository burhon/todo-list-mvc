package routes

import (
	"../controllers"
	"net/http"
)

func SetupRoutes() {
	http.HandleFunc("/delete/", controllers.TodoDelete)
	http.HandleFunc("/add",controllers.TodoAdd)
	http.HandleFunc("/edit",controllers.TodoEdit)
	http.HandleFunc("/", controllers.TodoList)
	http.ListenAndServe(":80", nil)
}
