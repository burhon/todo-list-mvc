package connection

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	DB_HOST     = "localhost"
	DB_USER     = "postgres"
	DB_PASSWORD = "1"
	DB_NAME     = "internship"
)

func SetupDB() *sql.DB {
	dbinfo := fmt.Sprintf("host=%s port=5432 user=%s password=%s dbname=%s sslmode=disable",DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", dbinfo)
	checkErr(err)

	return db
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
