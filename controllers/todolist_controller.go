package controllers

import (
	pgsqldb "../connection"
	"../models"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"
)

func TodoEdit( w http.ResponseWriter, r *http.Request)  {
	db := pgsqldb.SetupDB()
	tmpl := template.Must(template.ParseFiles("templates/TodoList.html"))
	key:=r.URL.Query().Get("id")

	if r.Method != http.MethodPost {
		sqlStatement := `
		SELECT *
		FROM todos
		WHERE id = $1;`
		rows,err := db.Query(sqlStatement,key)
		CheckError(err)
		var title string;
		var id int;
		var isDone bool;
		var created time.Time
		data:= models.TodoEdit{}
		for rows.Next() {
			err := rows.Scan(&id,&title,&isDone,&created)
			CheckError(err)
			data.ID=id
			data.Title=title
			data.IsDone=isDone
			data.Edit=true
			fmt.Println(title)
		}

		tmpl.Execute(w, data)
		return
	}
	sqlStatement := `
		UPDATE todos
		SET title = $2
		WHERE id = $1;`
	_,err := db.Exec(sqlStatement,key,r.FormValue("title"))
	CheckError(err)

	http.Redirect(w,r,"/",http.StatusSeeOther)
}


func TodoAdd(w http.ResponseWriter, r *http.Request)  {
	db := pgsqldb.SetupDB()
	tmpl := template.Must(template.ParseFiles("templates/TodoList.html"))
	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}

	details := models.TodoAdd{
		Title:   r.FormValue("title"),
	}
	sqlStatement := `
INSERT INTO todos ( title, is_done, created)
VALUES ( $1, $2, $3)`
	//var id int
	_,err:=db.Exec(sqlStatement, details.Title, false,time.Now().Format("2006.01.02 15:04:05.000"))

	CheckError(err)
	http.Redirect(w,r,"/",http.StatusSeeOther)
}

func TodoDelete(w http.ResponseWriter, r *http.Request)  {
	db := pgsqldb.SetupDB()
	//fmt.Println("# Querying",db)
	key:=r.URL.Query().Get("id")

	_, err := db.Exec("DELETE FROM todos WHERE id=$1", key)
	CheckError(err)
	http.Redirect(w,r,"/",http.StatusSeeOther)
}

func TodoList(w http.ResponseWriter, r *http.Request) {
	db := pgsqldb.SetupDB()
	fmt.Println("# Querying")
	rows,err  := db.Query("SELECT * FROM todos")
	CheckError(err)
	todoList:= models.Todo{}
	res:=[]models.Todo{};

	for rows.Next() {
		var title string;
		var created time.Time;
		var id int;
		var isDone bool;

		err = rows.Scan(&id,&title,&isDone,&created)
		CheckError(err)
		todoList.Title=title
		todoList.Created=created
		todoList.ID=id
		todoList.IsDone=isDone
		res = append(res,todoList)
		fmt.Println(res)
	}
	data:= models.TodoPageData{
		PageTitle:"TODO LIST",
		Edit:false,
		Todos:res,
	}
	tmpl := template.Must(template.ParseFiles("templates/TodoList.html"))

	tmpl.Execute(w, data)
}

func CheckError(err error) {
	if err != nil {
		log.Fatal("Failed to open a DB connection: ", err)
	}
}
